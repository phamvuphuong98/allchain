import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { DAppProvider } from "@usedapp/core";
import { ColorModeScript } from "@chakra-ui/react";
import theme from "./theme";

ReactDOM.render(
  <React.StrictMode>
    <DAppProvider config={{}}>
    <ColorModeScript initialColorMode={theme.config.initialColorMode} />
      <App />
    </DAppProvider>
  </React.StrictMode>,
  document.getElementById("root")
);
