import { Button, useColorMode, ButtonProps, Box } from '@chakra-ui/react'
import { useCallback, useEffect } from 'react'
import { RiMoonFill, RiSunFill } from 'react-icons/ri'
import { parseCookies, destroyCookie, setCookie } from 'nookies'

interface IProps extends ButtonProps {
  showTitle?: boolean
}
export default function ToggleColorButton({ showTitle = false, ...rest }: IProps) {
  const { colorMode, toggleColorMode, setColorMode } = useColorMode()
  const title = !showTitle ? '' : colorMode === 'light' ? 'Dark Mode' : 'Light Mode'

  const handleSystemPrefersChange = useCallback((opDark: boolean) => {
    const mod = opDark ? 'dark' : 'light'
    setColorMode(mod)
    setCookie(undefined, 'last-sys-prefers-color-mode', mod, {
      maxAge: 60 * 60 * 24 * 30, // 30 dias
      path: '/', // quais rotas terão acesso - '/'=todas
    })
    destroyCookie(undefined, 'chakra-ui-color-mode')
  }, [])

  useEffect(() => {
    if ((window as any) !== 'undefined') {
      const appOption = parseCookies()['chakra-ui-color-mode']
      console.log('[ToggleThemeMode] localColorMode ', appOption)

      const sysMQLDark = window.matchMedia('(prefers-color-scheme: dark)')
      const actualPreference = sysMQLDark?.matches ? 'dark' : 'light'
      if (!appOption) {
        if (sysMQLDark) {
          handleSystemPrefersChange(sysMQLDark.matches)
        }
      } else if (actualPreference !== parseCookies()['last-sys-prefers-color-mode']) {
        handleSystemPrefersChange(actualPreference === 'dark')
      }
    }
  }, [])

  useEffect(() => {
    if ((window as any) !== 'undefined') {
      const mql = window.matchMedia('(prefers-color-scheme: dark)')
      console.log(' ---  addEventListener ---')
      mql.addEventListener('change', m => handleSystemPrefersChange(m.matches))
      return () => {
        console.log(' ---  removeEventListener ---')
        mql.removeEventListener('change', m => handleSystemPrefersChange(m.matches))
      }
    }
  }, [])
  return (
    <Button variant="unstyled"  onClick={toggleColorMode} {...rest}>
      {title}{' '}
      <Box ml={!showTitle ? '' : '3'}>
        {colorMode === 'light' ? <RiMoonFill /> : <RiSunFill />}
      </Box>
    </Button>
  )
}
