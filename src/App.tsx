import { Box, ChakraProvider, Flex, Heading, HStack, Image, Spacer, Text, useDisclosure, VStack } from "@chakra-ui/react";
import theme from "./theme";
import ConnectButton from "./components/ConnectButton";
import AccountModal from "./components/AccountModal";
import ListChain from "./components/ListChain";
import ToggleColorButton from "./components/ToggleColorButton";
import "@fontsource/inter";
import AOS from "aos";
import "aos/dist/aos.css";
import { useEffect } from "react";

function App() {
  useEffect(() => {
    AOS.init();
    AOS.refresh();
  }, []);
  const { isOpen, onOpen, onClose } = useDisclosure();
  return (
    <ChakraProvider theme={theme}>
      <Flex padding='2'>
        <Box p='2'>
          <Heading as='h2' size='xl'>
            <a href="https://titans.ventures">
              Titans Ventures
            </a>
          </Heading>
        </Box>  
        <Spacer />
        <Box>
          <HStack spacing='24px'>
            <ConnectButton handleOpenModal={onOpen} />
            <AccountModal isOpen={isOpen} onClose={onClose} />
            <ToggleColorButton />
          </HStack>
        </Box>
      </Flex>
      <VStack>
        <ListChain />        
      </VStack>
      {/* <Layout>
        <ConnectButton handleOpenModal={onOpen} />
        <AccountModal isOpen={isOpen} onClose={onClose} />
      </Layout> */}
    </ChakraProvider>
  );
}

export default App;
